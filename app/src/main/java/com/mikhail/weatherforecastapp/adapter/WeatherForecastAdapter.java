package com.mikhail.weatherforecastapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhail.weatherforecastapp.R;
import com.mikhail.weatherforecastapp.model.WeatherDay;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class WeatherForecastAdapter extends RecyclerView.Adapter<WeatherForecastAdapter.ViewHolder> {

    private List<WeatherDay> weatherForecastList;

    public WeatherForecastAdapter(List<WeatherDay> weatherForecastList) {
        this.weatherForecastList = weatherForecastList;
    }

    public void setData(List<WeatherDay> weatherForecastList) {
        this.weatherForecastList = checkNotNull(weatherForecastList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E,MM.dd", Locale.ENGLISH);
        long date = weatherForecastList.get(position).dt;
        String formattedDayOfWeek = simpleDateFormat.format(date * 1000);
        float temperature = weatherForecastList.get(position).main.temp;

        holder.dateTextView.setText(formattedDayOfWeek);
        holder.temperatureTextView.setText(String.format("%d°", (int) temperature));
    }

    @Override
    public int getItemCount() {
        return weatherForecastList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateTextView;
        TextView temperatureTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.dateTextView = itemView.findViewById(R.id.date);
            this.temperatureTextView = itemView.findViewById(R.id.temperature);
        }
    }
}
