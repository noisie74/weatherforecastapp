package com.mikhail.weatherforecastapp.presenter;

import com.mikhail.weatherforecastapp.model.WeatherDay;
import com.mikhail.weatherforecastapp.model.WeatherForecast;
import com.mikhail.weatherforecastapp.service.WeatherForecastService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mikhail.weatherforecastapp.Util.API_KEY;
import static com.mikhail.weatherforecastapp.Util.UNITS;

public class WeatherForecastPresenter implements WeatherForecastContract.Presenter {

    private WeatherForecastContract.View mView;

    public WeatherForecastPresenter(WeatherForecastContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadWeatherForecast() {

        String cityName = mView.getCityName();

        WeatherForecastService.WeatherApi weatherApi = WeatherForecastService.create();

        if (!cityName.isEmpty()) {
            Call<WeatherForecast> apiCall = weatherApi.getForecast(cityName, UNITS, API_KEY);

            mView.showProgressBar();

            apiCall.enqueue(new Callback<WeatherForecast>() {
                @Override
                public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                    WeatherForecast data = response.body();

                    if (data != null) {
                        List<WeatherDay> forecastList = data.getItems();
                        if (response.isSuccessful()) {
                            mView.hideProgressBar();
                            mView.showWeatherForecast(forecastList);
                        }
                    } else {
                        mView.showErrorMessage();
                        mView.hideProgressBar();
                    }
                }

                @Override
                public void onFailure(Call<WeatherForecast> call, Throwable t) {
                    mView.hideProgressBar();
                    mView.showErrorMessage();
                }
            });

        } else {
            mView.hideProgressBar();
            mView.showErrorMessage();
        }
    }

}
