package com.mikhail.weatherforecastapp.presenter;

import com.mikhail.weatherforecastapp.model.WeatherDay;

import java.util.List;

public interface WeatherForecastContract {

    interface View {

        String getCityName();

        void showWeatherForecast(List<WeatherDay> weatherForecastList);

        void showProgressBar();

        void hideProgressBar();

        void showErrorMessage();

    }

    interface Presenter {

        void loadWeatherForecast();

    }
}
