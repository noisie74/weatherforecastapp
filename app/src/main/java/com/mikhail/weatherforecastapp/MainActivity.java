package com.mikhail.weatherforecastapp;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhail.weatherforecastapp.model.WeatherDay;
import com.mikhail.weatherforecastapp.presenter.WeatherForecastContract;
import com.mikhail.weatherforecastapp.presenter.WeatherForecastPresenter;
import com.mikhail.weatherforecastapp.adapter.WeatherForecastAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements WeatherForecastContract.View {

    private FloatingActionButton getWeatherForecastButton;
    private EditText citySearchEditText;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private WeatherForecastPresenter mPresenter;
    private WeatherForecastAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        mAdapter = new WeatherForecastAdapter(new ArrayList<WeatherDay>());
        setRecyclerView();
        mPresenter = new WeatherForecastPresenter(this);
        setWeatherForecastButton();
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        citySearchEditText = findViewById(R.id.enter_city_edit_text);
        getWeatherForecastButton = findViewById(R.id.search_button);
        mProgressBar = findViewById(R.id.progress_bar);
        mRecyclerView = findViewById(R.id.recycler_view);
    }

    private void setRecyclerView() {
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setWeatherForecastButton() {
        getWeatherForecastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citySearchEditText.clearFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                mProgressBar.setVisibility(View.VISIBLE);
                mPresenter.loadWeatherForecast();
            }
        });
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("CityName", citySearchEditText.getText().toString());
    }

    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String instanceState = savedInstanceState.getString("CityName");
        citySearchEditText.setText(instanceState);
    }

    @Override
    public String getCityName() {
        return citySearchEditText.getText().toString();
    }

    @Override
    public void showWeatherForecast(List<WeatherDay> weatherForecastList) {
        mAdapter.setData(weatherForecastList);
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Unable to load weather!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
