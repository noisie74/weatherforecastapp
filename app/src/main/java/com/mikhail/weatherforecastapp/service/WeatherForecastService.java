package com.mikhail.weatherforecastapp.service;

import com.mikhail.weatherforecastapp.model.WeatherForecast;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.mikhail.weatherforecastapp.Util.BASE_URL;

public class WeatherForecastService {

    public interface WeatherApi {

        @GET("forecast")
        Call<WeatherForecast> getForecast(
                @Query("q") String cityName,
                @Query("units") String units,
                @Query("appid") String appid
        );
    }

    public static WeatherApi create() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(WeatherApi.class);
    }
}
